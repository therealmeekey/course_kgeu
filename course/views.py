from django.shortcuts import render, get_object_or_404, redirect
from course.models import Course, Record
from course.forms import CourseForm, RecordForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def course_list(request):
    courses = Course.objects.all()
    paginator = Paginator(courses, 8)
    page = request.GET.get('page')
    try:
        course = paginator.page(page)
    except PageNotAnInteger:
        course = paginator.page(1)
    except EmptyPage:
        course = paginator.page(paginator.num_pages)
    return render(request, 'course/course_list.html', {'course': course})


def my_course(request):
    student = Record.objects.filter(student__username=request.user)
    my_courses = Course.objects.filter(lecturer__username=request.user)
    paginator = Paginator(my_courses, 8)
    page = request.GET.get('page')
    try:
        my_course = paginator.page(page)
    except PageNotAnInteger:
        my_course = paginator.page(1)
    except EmptyPage:
        my_course = paginator.page(paginator.num_pages)
    return render(request, 'course/my_course.html', {'my_course': my_course})


def course_detail(request, pk):
    course = get_object_or_404(Course, pk=pk)
    records = Record.objects.filter(course=course)
    return render(request, 'course/course_detail.html', {'course': course, 'records': records})


def course_edit(request, pk):
    if request.user.has_perm('course.lecturer'):
        course = get_object_or_404(Course, pk=pk)
        if request.user == course.lecturer:
            if request.method == "POST":
                form = CourseForm(request.POST, request.FILES, instance=course)
                if form.is_valid():
                    course = form.save(commit=False)
                    course.lecturer = request.user
                    course.save()
                    return redirect('course_detail', pk=course.pk)
            else:
                form = CourseForm(instance=course)
            return render(request, 'course/course_edit.html', {'form': form})
        else:
            return redirect('course_detail', pk=course.pk)


def course_new(request):
    if request.user.has_perm('course.lecturer'):
        if request.method == "POST":
            form = CourseForm(request.POST, request.FILES)
            if form.is_valid():
                course = form.save(commit=False)
                course.lecturer = request.user
                course.save()
                return redirect('course_detail', pk=course.pk)
        else:
            form = CourseForm()
        return render(request, 'course/course_edit.html', {'form': form})


def record_to_course(request, pk):
    if request.user.has_perm('course.student'):
        course = get_object_or_404(Course, pk=pk)
        if request.method == "POST":
            form = RecordForm(request.POST, request.FILES)
            if form.is_valid():
                record = form.save(commit=False)
                record.student = request.user
                record.course = course
                record.save()
                return redirect('course_detail', pk=course.pk)
        else:
            form = RecordForm()
        return render(request, 'course/record_to_course.html', {'form': form})


def search(request):
    error = False
    if 'q' in request.GET:
        q = request.GET['q']
        if not q:
            error = True
        else:
            course = Course.objects.filter(title__icontains=q)
            return render(request, 'course/course_list.html', {'course': course})
    return render(request, 'course/course_list.html', {'error': error})
