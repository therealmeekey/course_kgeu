from django.urls import path
from course import views

urlpatterns = [
    path('course_list/', views.course_list, name='course_list'),
    path('course/<int:pk>/', views.course_detail, name='course_detail'),
    path('my_course/', views.my_course, name='my_course'),
    path('course/<int:pk>/edit/', views.course_edit, name='course_edit'),
    path('course_new/', views.course_new, name='course_new'),
    path('course/<int:pk>/record', views.record_to_course, name='record_to_course'),
    path('course_list/search/', views.search),
]
