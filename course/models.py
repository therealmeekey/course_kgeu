from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import User
from .validators import validate_file_extension
from django.contrib.auth.models import Group, Permission, User


class Course(models.Model):
    title = models.CharField(max_length=128, verbose_name='Название', unique=True)
    description = models.TextField(verbose_name='Описание')
    price = models.PositiveIntegerField(validators=[MinValueValidator(0)], verbose_name='Цена')
    start_date = models.DateField(verbose_name='Начало курса')
    duration = models.PositiveIntegerField(validators=[MinValueValidator(0)], verbose_name='Колличество занятий')
    lesson_duration = models.PositiveIntegerField(blank=True, null=True,
                                                  validators=[MinValueValidator(0), MaxValueValidator(24)],
                                                  verbose_name='Продолжительность одного занятия(час)')
    image = models.ImageField(blank=True, null=True, upload_to='course_images', default='course_images/default.png',
                              verbose_name='Обложка')
    contract = models.FileField(blank=True, null=True, upload_to="documents/contract", verbose_name='Договор')
    invoice = models.FileField(blank=True, null=True, upload_to="documents/invoice", verbose_name='Квитанция')
    work_program = models.FileField(blank=True, null=True, upload_to="documents/work_program",
                                    verbose_name='Рабочая программа')
    lecturer = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Преподователь')

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'
        ordering = ['-id']
        permissions = (
            ("student", "Студент"),
            ("lecturer", "Преподователь")
        )

    def publish(self):
        self.start_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Institute(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название')

    class Meta:
        verbose_name = 'Институт'
        verbose_name_plural = 'Институты'

    def __str__(self):
        return self.title


class Department(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название')
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, verbose_name='Институт', null=True)

    class Meta:
        verbose_name = 'Кафедра'
        verbose_name_plural = 'Кафедры'

    def __str__(self):
        return self.title


class StudyGroup(models.Model):
    title = models.CharField(max_length=16, verbose_name='Название')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, verbose_name='Кафедра', null=True)

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'

    def __str__(self):
        return self.title


class Record(models.Model):
    student = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, verbose_name='Имя')
    surname = models.CharField(max_length=64, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=64, null=True, verbose_name='Отчество')
    date_birth = models.DateField(null=True, verbose_name='Дата Рождения')
    phone = models.CharField(max_length=11, null=True, verbose_name='Телефон')
    email = models.EmailField(default='', verbose_name='E-mail')
    institute = models.ForeignKey(Institute, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Институт')
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Кафедра')
    group = models.ForeignKey(StudyGroup, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Группа')
    contract = models.FileField(upload_to="record/documents/contract", validators=[validate_file_extension], null=True,
                                verbose_name='Договор')
    invoice = models.FileField(upload_to="record/documents/invoice", validators=[validate_file_extension], null=True,
                               verbose_name='Квитанция')

    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'

    def __str__(self):
        return '{} {}({}) записался на курс {}'.format(self.name, self.surname, self.student, self.course)
