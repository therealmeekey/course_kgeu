from django.contrib import admin
from course.models import Course, StudyGroup, Institute, Department, Record
from django.contrib.sites.models import Site


class CourseAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'lecturer')
    list_filter = ('title', 'lecturer')
    search_fields = ('title',)


admin.site.site_header = 'Сайт Администратора'
admin.site.site_title = 'Сайт Администратора'
admin.site.index_title = 'Панель Администратора'
admin.site.site_url = '/course_list/'
admin.site.register(Course, CourseAdmin)
admin.site.register(Institute)
admin.site.register(Department)
admin.site.register(StudyGroup)
admin.site.register(Record)
admin.site.unregister(Site)
