from django import forms
from course.models import Course, Record, Institute, Department, StudyGroup


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = (
            'title', 'description', 'price', 'start_date', 'duration', 'lesson_duration', 'contract', 'invoice',
            'image', 'work_program')
        widget = {'contract': forms.FileInput()}

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['description'].widget.attrs.update({'class': 'materialize-textarea'})
        self.fields['start_date'].widget.attrs.update({'class': 'datepicker'})


class RecordForm(forms.ModelForm):
    class Meta:
        model = Record
        fields = (
            'name', 'surname', 'patronymic', 'date_birth', 'phone', 'email', 'institute', 'department', 'group',
            'contract', 'invoice')

    def __init__(self, *args, **kwargs):
        super(RecordForm, self).__init__(*args, **kwargs)
        self.fields['date_birth'].widget.attrs.update({'class': 'datepicker'})
        self.fields['institute'].queryset = Institute.objects.all()
        self.fields['department'].queryset = Department.objects.all()
        self.fields['group'].queryset = StudyGroup.objects.all()
