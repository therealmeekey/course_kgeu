from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from courses import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from courses import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('course.urls')),
    path('', views.index),
    path('account/', include('profile.urls')),
    path('accounts/', include('allauth.urls')),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
