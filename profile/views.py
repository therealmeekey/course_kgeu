from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from profile.forms import UserForm, ProfileForm
from profile.models import User

# # Изменение профиля
#
# def edit_profile(request, username):
#     if request.method == 'POST':
#         user_form = UserForm(request.POST, instance=request.user)
#         profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
#         if user_form.is_valid() and profile_form.is_valid():
#             user_form.save()
#             profile_form.save()
#             messages.success(
#                 request,
#                 "Информация профиля успешно отредактирована!"
#             )
#             return HttpResponseRedirect(reverse('profile', kwargs={'username': request.user.username}))
#     else:
#         user = User.objects.get(username=username)
#         user_form = UserForm(instance=user)
#         profile_form = ProfileForm(instance=user.profile)
#     return render(request, 'account/profile_edit.html', {
#         'user_form': user_form,
#         'profile_form': profile_form
#     })


# Просмотр профиля


def view_profile(request, username):
    user = User.objects.get(username=username)
    return render(request, 'account/profile.html', {'user': user, })
