from django.urls import path
from profile import views

urlpatterns = [
    # path('profile_edit/<str:username>', views.edit_profile, name='edit_profile'),
    path('profile/<str:username>', views.view_profile, name='profile'),
]